class Timex
  MIN_PER_DAY = 24 * 60
  MIN_PER_HALF_DAY = MIN_PER_DAY / 2

  attr_reader :hours, :minutes, :period

  def initialize(time)
    if time.is_a?(String)
      hours, minutes, period = parseTime(time)
      @minutesSinceMidnight = minutesSinceMidnight(hours, minutes, period)
    elsif time.is_a?(Integer)
      @minutesSinceMidnight = time
    end
  end

  def +(min)
    minutes = @minutesSinceMidnight + min
    while minutes > MIN_PER_DAY do
      minutes -= MIN_PER_DAY
    end
    self.class.new(minutes)
  end

  def hours()
    hours = @minutesSinceMidnight / 60 % 12
    hours.zero?() ? 12 : hours
  end

  def minutes()
    @minutesSinceMidnight % 60
  end

  def period()
    @minutesSinceMidnight === MIN_PER_DAY || @minutesSinceMidnight < MIN_PER_HALF_DAY ? 'AM' : 'PM'
  end

  def to_s()
    sprintf("%d:%02d %s", hours(), minutes(), period())
  end

  private

  def parseTime(time)
    h, m, f = time.split(/:|\s/)
    hours, minutes = h.to_i, m.to_i
    period = f == 'PM' ? 1 : 0
    [hours, minutes, period]
  end

  def minutesSinceMidnight(hours, minutes, period)
    hours * 60 % MIN_PER_HALF_DAY + (MIN_PER_HALF_DAY * period) + minutes
  end
end
