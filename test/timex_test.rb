require_relative 'test_helper'
require_relative '../lib/timex'

describe 'Timex' do
  before do
    @t0 = Timex.new('10:30 AM')
    @t1 = Timex.new('12:59 AM')
    @t2 = Timex.new('11:59 AM')
    @t3 = Timex.new('12:59 PM')
    @t4 = Timex.new('11:59 PM')
  end

  def test_timex_class_exists
    assert Timex.class
  end

  def test_hours
    assert_equal(12, @t1.hours())
  end

  def test_minutes
    assert_equal(59, @t1.minutes())
  end

  def test_am_period
    assert_equal('AM', @t1.period())
  end

  def test_pm_period
    assert_equal('PM', @t3.period())
  end

  def test_add_creates_new_instance
    t = @t1 + 5
    assert_equal('12:59 AM', @t1.to_s)
    refute_equal(t.to_s, @t1.to_s)
    assert_kind_of(Timex, t)
  end

  def test_add_minutes
    t = @t0 + 5
    assert_equal('10:35 AM', t.to_s)
  end

  def test_am_to_pm_transition
    t = @t2 + 1
    assert_equal('12:00 PM', t.to_s)
  end

  def test_pm_to_am_transition
    t = @t4 + 1
    assert_equal('12:00 AM', t.to_s)
  end

  def test_12_to_1_am_transition
    t = @t1 + 1
    assert_equal('1:00 AM', t.to_s)
  end

  def test_12_to_1_pm_transition
    t = @t3 + 1
    assert_equal('1:00 PM', t.to_s)
  end

  def test_add_negative_minutes
    t = @t4 + -4
    assert_equal('11:55 PM', t.to_s)
  end

  def test_minutes_exceed_day
    min = (24 * 60 * 1.5).to_i
    t = Timex.new(min)
    assert_equal('12:00 PM', t.to_s)
  end
end

