### Clock
The clock class has a single class method named add_minutes, which accepts 2 arguments. The first is a string containing
a time in 12-hour format, e.g. `'[N]N:NN AM|PM'`. The second is an integer representing the number of minutes to add to
the time specifed in the first argument. The modified time is returned in a 12-hour clock format.

### Running the app
• Note: These instruction assume Git, Ruby, and the Bundler gem have already been installed

• Clone this Git repository

• Navigate to the directory housing the newly cloned repository

• Install the dependencies using bundler: `bundle install`

• Run the included unit tests using the rake task: `rake test`

• The clock class' add_minutes method can be invoked by calling `Clock.add_minutes(time, minutes)`
